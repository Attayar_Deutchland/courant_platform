<?php

namespace App\Http\Requests;

use App\RapportFinancier;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRapportFinancierRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('rapport_financier_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:rapport_financiers,id',
        ];
    }
}
