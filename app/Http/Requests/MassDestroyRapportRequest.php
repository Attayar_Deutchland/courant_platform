<?php

namespace App\Http\Requests;

use App\Rapport;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRapportRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('rapport_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:rapports,id',
        ];
    }
}
