<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyRapportFinancierRequest;
use App\Http\Requests\StoreRapportFinancierRequest;
use App\Http\Requests\UpdateRapportFinancierRequest;
use App\RapportFinancier;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RapportFinancierController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('rapport_financier_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rapportFinanciers = RapportFinancier::all();

        return view('admin.rapportFinanciers.index', compact('rapportFinanciers'));
    }

    public function create()
    {
        abort_if(Gate::denies('rapport_financier_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rapportFinanciers.create');
    }

    public function store(StoreRapportFinancierRequest $request)
    {
        $rapportFinancier = RapportFinancier::create($request->all());

        if ($request->input('file', false)) {
            $rapportFinancier->addMedia(storage_path('tmp/uploads/' . $request->input('file')))->toMediaCollection('file');
        }

        return redirect()->route('admin.rapport-financiers.index');
    }

    public function edit(RapportFinancier $rapportFinancier)
    {
        abort_if(Gate::denies('rapport_financier_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rapportFinanciers.edit', compact('rapportFinancier'));
    }

    public function update(UpdateRapportFinancierRequest $request, RapportFinancier $rapportFinancier)
    {
        $rapportFinancier->update($request->all());

        if ($request->input('file', false)) {
            if (!$rapportFinancier->file || $request->input('file') !== $rapportFinancier->file->file_name) {
                $rapportFinancier->addMedia(storage_path('tmp/uploads/' . $request->input('file')))->toMediaCollection('file');
            }
        } elseif ($rapportFinancier->file) {
            $rapportFinancier->file->delete();
        }

        return redirect()->route('admin.rapport-financiers.index');
    }

    public function show(RapportFinancier $rapportFinancier)
    {
        abort_if(Gate::denies('rapport_financier_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rapportFinanciers.show', compact('rapportFinancier'));
    }

    public function destroy(RapportFinancier $rapportFinancier)
    {
        abort_if(Gate::denies('rapport_financier_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rapportFinancier->delete();

        return back();
    }

    public function massDestroy(MassDestroyRapportFinancierRequest $request)
    {
        RapportFinancier::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
