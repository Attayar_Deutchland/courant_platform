<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Profile extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'profiles';

    protected $appends = [
        'photo',
    ];

    const SEXE_SELECT = [
        'Homme' => '',
        'Femme' => '',
    ];

    const STATUS_SELECT = [
        'Etudiant'  => '',
        'Employee'  => '',
        'Chomeur'   => '',
        'Formation' => '',
    ];

    protected $dates = [
        'date_end',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
        'date_of_birthday',
    ];

    protected $fillable = [
        'sexe',
        'active',
        'status',
        'address',
        'user_id',
        'lastname',
        'date_end',
        'firstname',
        'telephone',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
        'date_of_birthday',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getDateOfBirthdayAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setDateOfBirthdayAttribute($value)
    {
        $this->attributes['date_of_birthday'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function getPhotoAttribute()
    {
        $file = $this->getMedia('photo')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }

    public function getDateBeginnAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setDateBeginnAttribute($value)
    {
        $this->attributes['date_beginn'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function getDateEndAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setDateEndAttribute($value)
    {
        $this->attributes['date_end'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
