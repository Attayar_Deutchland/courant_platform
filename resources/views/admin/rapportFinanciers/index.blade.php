@extends('layouts.admin')
@section('content')
@can('rapport_financier_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.rapport-financiers.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.rapportFinancier.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.rapportFinancier.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-RapportFinancier">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.rapportFinancier.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.rapportFinancier.fields.title') }}
                        </th>
                        <th>
                            {{ trans('cruds.rapportFinancier.fields.file') }}
                        </th>
                        <th>
                            {{ trans('cruds.rapportFinancier.fields.date') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rapportFinanciers as $key => $rapportFinancier)
                        <tr data-entry-id="{{ $rapportFinancier->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $rapportFinancier->id ?? '' }}
                            </td>
                            <td>
                                {{ $rapportFinancier->title ?? '' }}
                            </td>
                            <td>
                                @if($rapportFinancier->file)
                                    <a href="{{ $rapportFinancier->file->getUrl() }}" target="_blank">
                                        {{ trans('global.view_file') }}
                                    </a>
                                @endif
                            </td>
                            <td>
                                {{ $rapportFinancier->date ?? '' }}
                            </td>
                            <td>
                                @can('rapport_financier_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.rapport-financiers.show', $rapportFinancier->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('rapport_financier_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.rapport-financiers.edit', $rapportFinancier->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('rapport_financier_delete')
                                    <form action="{{ route('admin.rapport-financiers.destroy', $rapportFinancier->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('rapport_financier_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.rapport-financiers.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-RapportFinancier:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection