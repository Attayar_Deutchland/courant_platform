@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.rapportFinancier.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.rapport-financiers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.rapportFinancier.fields.id') }}
                        </th>
                        <td>
                            {{ $rapportFinancier->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.rapportFinancier.fields.title') }}
                        </th>
                        <td>
                            {{ $rapportFinancier->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.rapportFinancier.fields.file') }}
                        </th>
                        <td>
                            @if($rapportFinancier->file)
                                <a href="{{ $rapportFinancier->file->getUrl() }}" target="_blank">
                                    {{ trans('global.view_file') }}
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.rapportFinancier.fields.date') }}
                        </th>
                        <td>
                            {{ $rapportFinancier->date }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.rapport-financiers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection