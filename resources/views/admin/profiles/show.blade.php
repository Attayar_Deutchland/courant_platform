@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.profile.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.profiles.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.profile.fields.id') }}
                        </th>
                        <td>
                            {{ $profile->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.profile.fields.firstname') }}
                        </th>
                        <td>
                            {{ $profile->firstname }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.profile.fields.lastname') }}
                        </th>
                        <td>
                            {{ $profile->lastname }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.profile.fields.date_of_birthday') }}
                        </th>
                        <td>
                            {{ $profile->date_of_birthday }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.profile.fields.address') }}
                        </th>
                        <td>
                            {{ $profile->address }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.profile.fields.telephone') }}
                        </th>
                        <td>
                            {{ $profile->telephone }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.profile.fields.photo') }}
                        </th>
                        <td>
                            @if($profile->photo)
                                <a href="{{ $profile->photo->getUrl() }}" target="_blank">
                                    <img src="{{ $profile->photo->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.profile.fields.date_beginn') }}
                        </th>
                        <td>
                            {{ $profile->date_beginn }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.profile.fields.date_end') }}
                        </th>
                        <td>
                            {{ $profile->date_end }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.profile.fields.active') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $profile->active ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.profile.fields.sexe') }}
                        </th>
                        <td>
                            {{ App\Profile::SEXE_SELECT[$profile->sexe] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.profile.fields.status') }}
                        </th>
                        <td>
                            {{ App\Profile::STATUS_SELECT[$profile->status] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.profile.fields.user') }}
                        </th>
                        <td>
                            {{ $profile->user->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.profiles.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection