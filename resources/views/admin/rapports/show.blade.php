@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.rapport.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.rapports.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.rapport.fields.id') }}
                        </th>
                        <td>
                            {{ $rapport->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.rapport.fields.title') }}
                        </th>
                        <td>
                            {{ $rapport->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.rapport.fields.file') }}
                        </th>
                        <td>
                            @if($rapport->file)
                                <a href="{{ $rapport->file->getUrl() }}" target="_blank">
                                    {{ trans('global.view_file') }}
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.rapport.fields.date') }}
                        </th>
                        <td>
                            {{ $rapport->date }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.rapports.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection