<?php

return [
    'date_format'               => 'Y-m-d',
    'time_format'               => 'H:i:s',
    'primary_language'          => 'fr',
    'available_languages'       => [
        'fr'      => 'French',
        'en'      => 'English',
        'ru'      => 'Russian',
        'es'      => 'Spanish',
        'ar'      => 'Arabic',
        'de'      => 'German',
        'it'      => 'Italian',
        'zh-Hans' => 'Chinese',
    ],
    'registration_default_role' => '2',
];
