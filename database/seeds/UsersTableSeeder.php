<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Aouinti',
                'email'          => 'rami.aouinti@gmail.com',
                'password'       => '$2y$10$MuHEbGQx5rVwC/d8HRraJe1PKQjhmheOwtY2nMu6KXjSNWYKiMs6O',
                'remember_token' => null,
                'approved'       => 1,
            ],
        ];

        User::insert($users);
    }
}
