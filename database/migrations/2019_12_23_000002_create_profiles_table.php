<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');

            $table->string('firstname')->nullable();

            $table->string('lastname')->nullable();

            $table->date('date_of_birthday')->nullable();

            $table->string('address')->nullable();

            $table->string('telephone')->nullable();

            $table->date('date_beginn')->nullable();

            $table->date('date_end')->nullable();

            $table->boolean('active')->default(0)->nullable();

            $table->string('sexe')->nullable();

            $table->string('status')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
