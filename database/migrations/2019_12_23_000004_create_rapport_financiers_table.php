<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRapportFinanciersTable extends Migration
{
    public function up()
    {
        Schema::create('rapport_financiers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')->nullable();

            $table->date('date')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
