<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRapportsTable extends Migration
{
    public function up()
    {
        Schema::create('rapports', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')->nullable();

            $table->date('date')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
